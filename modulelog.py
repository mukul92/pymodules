#
# PyModules - Software Environments for Research Computing Clusters
#
# Copyright 2012-2013, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of PyModules.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose other than its incorporation into a
# commercial product is hereby granted without fee, provided that the
# above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation, and that the name of Brown University not be used in
# advertising or publicity pertaining to distribution of the software
# without specific, written prior permission.
#
# BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
# PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
# ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


import logging
import os
import sys
# import sqlite3 as sqlite
# from cloghandler import ConcurrentRotatingFileHandler as UnifiedRotatingFileHandler
from datetime import datetime
from logging.handlers import RotatingFileHandler
from modulecfg import *
from moduleutil import get_username, get_hostname

# Now check if mysql.connector available and import
# Turn off logging if not available
import imp
try:
    mysql_info = imp.find_module('mysql')
    mysql = imp.load_module('mysql',*mysql_info)
    imp.find_module('connector', mysql.__path__)
    import mysql.connector
except ImportError:
   modulelog_config['enabled'] = False


SUPPRESS_EXCEPTIONS = modulelog_config['suppress-error']
TIMEOUT = modulelog_config['conn-timeout']


class ModuleLogError(Exception):
    """ Base class for all modulelog exceptions """
    def __init__(self, msg, type=None):
        super(ModuleLogError,self).__init__(msg)
        self.warning = msg + messages.get(type, '')

    def warn(self):
        """ Print a warning message. """
        print >>sys.stderr, "modulelog: warning:", self.warning


class RotatingFileLogAdapter:

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        log_handler = UnifiedRotatingFileHandler(os.path.join(modulelog_config['filelog-path'], 'pymodule.log'), maxBytes=5*1024*1024, backupCount=3)
        log_handler.setFormatter(logging.Formatter(fmt="%(asctime)-15s %(host)s %(user)-8s %(message)s"))
        self.logger.addHandler(log_handler)
        self.logger.setLevel(logging.INFO)
        self.log_params = log_params

    def log(self, operation, internal, module, version, timestamp, remark=''):
        internal_label = "(internal)" if internal else ""
        self.logger.info('module %s "%s/%s %s < %s (%s)"', operation, module, version, internal_label, MODULECALLER, timestamp, extra=self.log_params)

    def close(self):
        pass


class SqliteLogAdapter:

    def __init__(self, log_params={}):
        self.conn = None
        self.connect(os.path.join(modulelog_config['sqlite-path'], 'pymodule.log.sqlite'))
        self.log_params = log_params

    def connect(self, dbfile):
        """ Initializes connections to the sqlite database with autocommit mode """
        try:
            if self.conn:
                self.conn.close()
            self.conn = sqlite.connect(dbfile, isolation_level=None, timeout=TIMEOUT)
        except sqlite.OperationalError as e:
            raise ModuleLogError("can't connect to database '%s' (sqlite3 error: %s)" % (dbfile, e))

        # initialize the database if it is new
        self.conn.execute("""
            CREATE TABLE IF NOT EXISTS `modulelog` (
	            `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	            `module`	TEXT NOT NULL,
	            `version`	TEXT NOT NULL,
	            `operation`	TEXT NOT NULL,
	            `user`	TEXT,
	            `host`	TEXT,
	            `internal`	NUMERIC NOT NULL DEFAULT 0,
	            `caller`	TEXT NOT NULL,
	            `utctime`	NUMERIC NOT NULL,
	            `logged_utctime`	NUMERIC NOT NULL,
	            `remark`	TEXT NOT NULL
            );""")

    def log(self, operation, internal, module, version, timestamp, remark=''):
        self.conn.execute("""
            INSERT INTO `modulelog` (
                `operation`,
                `internal`,
                `module`,
                `version`,
                `user`,
                `host`,
                `caller`,
                `utctime`,
                `logged_utctime`,
                `remark`
            ) VALUES (?,?,?,?,?,?,?,?,?,?)""",
            (operation, internal, module, version, self.log_params['user'], self.log_params['host'], MODULECALLER, timestamp, datetime.utcnow(), remark))

    def close(self):
        self.conn.close()


class MysqlLogAdapter:

    def __init__(self, log_params={}):
        self.conn = None
        self.connect(modulelog_config)
        self.log_params = log_params

    def connect(self, config):
        """ Initializes connections to the MySQL database with autocommit mode """
        try:
            if self.conn:
                self.conn.close()
            self.conn = mysql.connector.connect(
                user=config['mysql-user'],
                password=config['mysql-pass'],
                host=config['mysql-host'],
                database=config['mysql-db'],
                connection_timeout=TIMEOUT,
                autocommit=True
            )
        except mysql.connector.Error as e:
            raise ModuleLogError("can't connect to database (mysql connector error: %s)" % e)

    def log(self, operation, internal, module, version, timestamp, remark=''):
        self.conn.cursor().execute("""
            INSERT INTO `modulelog` (
                `operation`,
                `internal`,
                `module`,
                `version`,
                `user`,
                `host`,
                `caller`,
                `utctime`,
                `logged_utctime`,
                `remark`
            ) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
            (operation, internal, module, version, self.log_params['user'], self.log_params['host'], MODULECALLER, timestamp, datetime.utcnow(), remark))

    def close(self):
        self.conn.close()
        self.conn = None


class NullLogAdapter:

    def __init__(self, log_params={}):
        pass

    def log(self, operation, internal, module, version, timestamp, remark=''):
        pass

    def close(self):
        pass


class ModuleLogger:
    ''' A class does lazy logging support. This class is not thread-safe or reusable '''

    def __init__(self):
        self.request_list = []
        
        self.log_params = {}
        self.log_params['user'] = get_username()
        self.log_params['host'] = get_hostname()
        
        if ENABLE_MODULELOGFAILOUT and ENABLE_MODULELOG and modulelog_config.get('enabled', False):
            self.failout_logger = logging.getLogger(__name__)
            failout_log_handler = RotatingFileHandler(os.path.expanduser('~/.modulelog.log'), maxBytes=10*1024*1024, backupCount=3)
            failout_log_handler.setFormatter(logging.Formatter(fmt="%(asctime)-15s %(message)s"))
            self.failout_logger.addHandler(failout_log_handler)
            self.failout_logger.setLevel(logging.ERROR)

    def failoutLog(self, *args, **kargs):
        if ENABLE_MODULELOGFAILOUT and ENABLE_MODULELOG and modulelog_config.get('enabled', False):
            self.failout_logger.error(*args, **kargs)

    def failoutLogCurrentException(self):
        global SUPPRESS_EXCEPTIONS
        if ENABLE_MODULELOGFAILOUT and ENABLE_MODULELOG and modulelog_config.get('enabled', False):
            self.failoutLog('ModuleLogger failed', extra=self.log_params, exc_info=sys.exc_info())
            if not SUPPRESS_EXCEPTIONS:
                raise

    def newLogger(self):
        logAdapter = None
        try:
            if ENABLE_MODULELOG and modulelog_config.get('enabled', False):
                logAdapter = MysqlLogAdapter(self.log_params)
            else:
                logAdapter = NullLogAdapter(self.log_params)
        except SystemExit:
            raise
        except:
            logAdapter = NullLogAdapter(self.log_params)
            self.failoutLogCurrentException()
        return logAdapter

    def logLoad(self, module, version, internal=False):
        ''' Append the log request for load to the request list for lazy logging '''
        try:
            req = ('load', internal, module, version, datetime.utcnow())
            self.request_list.append(req)
        except SystemExit:
            raise
        except:
            self.failoutLogCurrentException()

    def logUnload(self, module, version, internal=False):
        ''' Append the log request for unload to the request list for lazy logging '''
        try:
            req = ('unload', internal, module, version, datetime.utcnow())
            self.request_list.append(req)
        except SystemExit:
            raise
        except:
            self.failoutLogCurrentException()

    def flush(self):
        ''' Write the log in a separate process'''
        # flush all I/O buffers for the fork
        sys.stdout.flush()
        sys.stderr.flush()
        try:
            pid = os.fork()
            if pid != 0:
                return
            else:
                # redirect stdout of the subprocess so that bash could return
                os.dup2(sys.stderr.fileno(), sys.stdout.fileno())
                self._sync()
                sys.exit(0)
        except SystemExit:
            raise
        except:
            self.failoutLogCurrentException()

    def _sync(self):
        try:
            if len(self.request_list) > 0:
                logger = self.newLogger()
                for req in self.request_list:
                    logger.log(*req)
                logger.close()
        except SystemExit:
            raise
        except:
            self.failoutLogCurrentException()


